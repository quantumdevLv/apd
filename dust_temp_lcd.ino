
#include <Wire.h>
#include "rgb_lcd.h"

#define DHT11_PIN 2      // ADC2


rgb_lcd lcd;

const int colorR = 255;
const int colorG = 255;
const int colorB = 255;

const int pinButton = 3;

int pin = 8;
unsigned long duration;
unsigned long starttime;
unsigned long sampletime_ms = 2000;//sampe 30s&nbsp;;
unsigned long lowpulseoccupancy = 0;
float ratio = 0;
float concentration = 0;



byte read_dht11_dat()
{
    byte i = 0;
    byte result=0;
    for(i=0; i< 8; i++){

        while(!(PINC & _BV(DHT11_PIN)));  // wait for 50us
        delayMicroseconds(30);

        if(PINC & _BV(DHT11_PIN))
        result |=(1<<(7-i));
        while((PINC & _BV(DHT11_PIN)));  // wait '1' finish
    }
    return result;
}


void setup() {
 lcd.begin(16, 2);  
 lcd.setRGB(colorR, colorG, colorB);
 
 DDRC |= _BV(DHT11_PIN);
 PORTC |= _BV(DHT11_PIN);
 
 Serial.begin(9600);
 pinMode(8,INPUT);
 starttime = millis();

 pinMode(pinButton, INPUT);

}

void loop(){
  //dust sensor
  duration = pulseIn(pin, LOW);
  lowpulseoccupancy = lowpulseoccupancy+duration;

  if ((millis()-starttime) >= sampletime_ms)//if the sampel time = = 30s
  {
    ratio = lowpulseoccupancy/(sampletime_ms*10.0);  // Integer percentage 0=&gt;100
    concentration = 1.1*pow(ratio,3)-3.8*pow(ratio,2)+520*ratio+0.62; // using spec sheet curve
    Serial.print("concent = ");
    Serial.print(concentration);
    Serial.println(" pcs/0.01cf");
    Serial.println("\n");
    lowpulseoccupancy = 0;
    starttime = millis();


//temp&humidity 

    byte dht11_dat[5];
    byte dht11_in;
    byte i;
    // start condition
    // 1. pull-down i/o pin from 18ms
    PORTC &= ~_BV(DHT11_PIN);
    delay(18);
    PORTC |= _BV(DHT11_PIN);
    delayMicroseconds(40);

    DDRC &= ~_BV(DHT11_PIN);
    delayMicroseconds(40);

    dht11_in = PINC & _BV(DHT11_PIN);

    if(dht11_in){
        Serial.println("dht11 start condition 1 not met");
        return;
    }
    delayMicroseconds(80);

    dht11_in = PINC & _BV(DHT11_PIN);

    if(!dht11_in){
        Serial.println("dht11 start condition 2 not met");
        return;
    }
    delayMicroseconds(80);
    // now ready for data reception
    for (i=0; i<5; i++)
    dht11_dat[i] = read_dht11_dat();

    DDRC |= _BV(DHT11_PIN);
    PORTC |= _BV(DHT11_PIN);

    byte dht11_check_sum = dht11_dat[0]+dht11_dat[1]+dht11_dat[2]+dht11_dat[3];
    // check check_sum
    if(dht11_dat[4]!= dht11_check_sum)
    {
        Serial.println("DHT11 checksum error");
    }



    if(digitalRead(pinButton)) 
    {
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("concen=");
      lcd.print(concentration);
      
      
    }
    else {
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("  RH = ");
      lcd.print(dht11_dat[0], DEC);
      lcd.print(".");
      lcd.print(dht11_dat[1],DEC);
      lcd.println("%");
      lcd.setCursor(0,1);
      lcd.print("temp = ");
      lcd.print(dht11_dat[2],DEC);
      lcd.print(".");
      lcd.print(dht11_dat[3],DEC);
      lcd.print("C ");
    }

  }
}
